﻿using System.ComponentModel.DataAnnotations;

namespace btg.Services.CouponAPI.Models
{
    public class Coupon
    {
        /// <summary>
        /// id
        /// </summary>
        [Key]
        public int CouponId { get; set; }
        /// <summary>
        /// ma khuyen mai
        /// </summary>
        [Required]
        public string CouponCode { get; set; } = "";
        /// <summary>
        /// so tien khuyen mai
        /// </summary>
        [Required]
        public double DiscountAmount { get; set; }
        /// <summary>
        /// so tien yeu cau
        /// </summary>
        public int MinAmount { get; set; }
        /// <summary>
        /// ngay tao
        /// </summary>
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
