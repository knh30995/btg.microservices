﻿namespace btg.Services.CouponAPI.Models.Dto
{
    public class CouponDto
    {
        /// <summary>
        /// id
        /// </summary>
        public int CouponId { get; set; }
        /// <summary>
        /// ma khuyen mai
        /// </summary>
        public string CouponCode { get; set; } = "";
        /// <summary>
        /// so tien khuyen mai
        /// </summary>
        public double DiscountAmount { get; set; }
        /// <summary>
        /// so tien yeu cau
        /// </summary>
        public int MinAmount { get; set; }
        /// <summary>
        /// ngay tao
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }
}
